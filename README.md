# hr_management

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.



# Project Documentation
## Getting started

This project flutter version is 3.13.1 and dart version is 3.1.0

This project include below functionality
- Drag & Drop functionality for Text widget, Basic and Advance Image Drag & drop functionality
- Drawer Appliction using Custom Paint
- Save yourDrawing in yout local storage(gallery)  

Here is the some screnshort for more understanding in this project

How image save in your local storage
<!-- ![Alt text](2023-08-25T14_55_55.295161.jpg) -->
<!-- ![Alt text](1692958949863.JPEG) -->