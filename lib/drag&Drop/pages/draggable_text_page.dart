import 'package:flutter/material.dart';
import 'package:hr_management/main.dart';
import 'package:hr_management/widget/common_widget.dart';

class DraggableTextPage extends StatelessWidget {
  const DraggableTextPage({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text(MyApp.title),
        ),
        drawer: const CommonDrawer(),
        body: Center(
          child: Draggable(
            feedback: Material(child: buildText('Dragged', Colors.green)),
            childWhenDragging: buildText('Behind', Colors.red),
            child: buildText('Drag Me', Colors.purple),
          ),
        ),
      );

  Widget buildText(String text, Color color) => Container(
        alignment: Alignment.center,
        width: 160,
        height: 100,
        color: color,
        child: Text(
          text,
          style: const TextStyle(color: Colors.white, fontSize: 32),
          textAlign: TextAlign.center,
        ),
      );
}
