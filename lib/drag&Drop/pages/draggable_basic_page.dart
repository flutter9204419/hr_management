// ignore_for_file: library_private_types_in_public_api

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:hr_management/drag&Drop/widget/app_bar_widget.dart';
import 'package:hr_management/widget/common_widget.dart';
import 'package:hr_management/drag&Drop/widget/draggable_widget.dart';
import '../data/data.dart';

class DraggableBasicPage extends StatefulWidget {
  const DraggableBasicPage({super.key});

  @override
  _DraggableBasicPageState createState() => _DraggableBasicPageState();
}

class _DraggableBasicPageState extends State<DraggableBasicPage> {
  List<Animal> all = allAnimals;
  int score = 0;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: buildAppBar(score: score),
        drawer: const CommonDrawer(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              buildOrigin(),
              buildTargets(context),
            ],
          ),
        ),
      );

  Widget buildTargets(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          buildTarget(
            context,
            text: 'Animals',
            acceptType: AnimalType.land,
          ),
          buildTarget(
            context,
            text: 'Birds',
            acceptType: AnimalType.air,
          ),
        ],
      );

  Widget buildOrigin() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Stack(
            alignment: Alignment.center,
            children:
                all.map((animal) => DraggableWidget(animal: animal)).toList(),
          ),
          TextButton(
            onPressed: () {
              setState(() {
                all = [];

                for (int i = 1; i <= 3; i++) {
                  all.add(Animal(
                    type: AnimalType.air,
                    imageUrl: 'assets/bird$i.png',
                  ));
                }
                for (int i = 1; i <= 3; i++) {
                  all.add(Animal(
                    type: AnimalType.land,
                    imageUrl: 'assets/animal$i.png',
                  ));
                }
                log("all $all and  $allAnimals");
                score = 0;
              });
            },
            child: const Text(
              "Reset Images",
            ),
          ),
        ],
      );

  Widget buildTarget(
    BuildContext context, {
    required String text,
    required AnimalType acceptType,
  }) =>
      CircleAvatar(
        radius: 75,
        backgroundColor: Theme.of(context).colorScheme.primary,
        child: DragTarget<Animal>(
          builder: (context, candidateData, rejectedData) => Center(
            child: Text(
              text,
              style: const TextStyle(color: Colors.white, fontSize: 24),
            ),
          ),
          onWillAccept: (data) => true,
          onAccept: (data) {
            if (data.type == acceptType) {
              // Utils.showSnackBar(
              //   context,
              //   text: 'This Is Correct 🥳',
              //   color: Colors.green,
              // );

              setState(() {
                score += 50;
                all.removeWhere((animal) => animal.imageUrl == data.imageUrl);
              });
            } else {
              setState(() => score -= 20);

              // Utils.showSnackBar(
              //   context,
              //   text: 'Try Again! 😥',
              //   color: Colors.red,
              // );
            }
          },
        ),
      );
}
