// ignore_for_file: library_private_types_in_public_api

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:hr_management/drag&Drop/data/data.dart';
import 'package:hr_management/main.dart';
import 'package:hr_management/widget/common_widget.dart';
import 'package:hr_management/drag&Drop/widget/draggable_widget.dart';

class DraggableAdvancedPage extends StatefulWidget {
  const DraggableAdvancedPage({super.key});

  @override
  _DraggableAdvancedPageState createState() => _DraggableAdvancedPageState();
}

class _DraggableAdvancedPageState extends State<DraggableAdvancedPage> {
  List<Animal> all = allAnimals;
  List<Animal> land = [];
  List<Animal> air = [];

  final double size = 150;

  void removeAll(Animal toRemove) {
    all.removeWhere((animal) => animal.imageUrl == toRemove.imageUrl);
    land.removeWhere((animal) => animal.imageUrl == toRemove.imageUrl);
    air.removeWhere((animal) => animal.imageUrl == toRemove.imageUrl);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text(MyApp.title),
          actions: [
            IconButton(
                onPressed: () {
                  setState(() {
                    all = [];

                    for (int i = 1; i <= 3; i++) {
                      all.add(Animal(
                        type: AnimalType.air,
                        imageUrl: 'assets/bird$i.png',
                      ));
                    }
                    for (int i = 1; i <= 3; i++) {
                      all.add(Animal(
                        type: AnimalType.land,
                        imageUrl: 'assets/animal$i.png',
                      ));
                    }
                    log("all $all and  $allAnimals");
                    land = [];
                    air = [];
                  });
                },
                icon: const Icon(Icons.image)),
          ],
        ),
        drawer: const CommonDrawer(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            buildTarget(
              context,
              text: 'All',
              animals: all,
              acceptTypes: AnimalType.values,
              onAccept: (data) => setState(() {
                removeAll(data);
                all.add(data);
              }),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                buildTarget(
                  context,
                  text: 'Animals',
                  animals: land,
                  acceptTypes: [AnimalType.land],
                  onAccept: (data) => setState(() {
                    removeAll(data);
                    land.add(data);
                  }),
                ),
                buildTarget(
                  context,
                  text: 'Birds',
                  animals: air,
                  acceptTypes: [AnimalType.air],
                  onAccept: (data) => setState(() {
                    removeAll(data);
                    air.add(data);
                  }),
                ),
              ],
            ),
          ],
        ),
      );

  Widget buildTarget(
    BuildContext context, {
    required String text,
    required List<Animal> animals,
    required List<AnimalType> acceptTypes,
    required DragTargetAccept<Animal> onAccept,
  }) =>
      CircleAvatar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        radius: size / 2,
        child: DragTarget<Animal>(
          builder: (context, candidateData, rejectedData) => Stack(
            children: [
              ...animals
                  .map((animal) => DraggableWidget(animal: animal))
                  .toList(),
              IgnorePointer(child: Center(child: buildText(text))),
            ],
          ),
          onWillAccept: (data) => true,
          onAccept: (data) {
            if (acceptTypes.contains(data.type)) {
              // Utils.showSnackBar(
              //   context,
              //   text: 'This Is Correct 🥳',
              //   color: Colors.green,
              // );
            } else {
              // Utils.showSnackBar(
              //   context,
              //   text: 'This Looks Wrong 🤔',
              //   color: Colors.red,
              // );
            }

            onAccept(data);
          },
        ),
      );

  Widget buildText(String text) => Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.8),
            blurRadius: 12,
          )
        ]),
        child: Text(
          text,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
}
