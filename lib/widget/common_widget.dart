import 'package:flutter/material.dart';
import 'package:hr_management/drag&Drop/pages/draggable_advanced_page.dart';
import 'package:hr_management/drag&Drop/pages/draggable_basic_page.dart';
import 'package:hr_management/drag&Drop/pages/draggable_text_page.dart';
import 'package:hr_management/drawingApp/pages/drawing_page.dart';
import 'package:hr_management/imageEditor/pages/edit_image_final.dart';

class CommonDrawer extends StatelessWidget {
  const CommonDrawer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Drawer(
        // backgroundColor: Colors.black54,
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
          // margin: EdgeInsets.zero,
          // decoration: BoxDecoration(
          //   // color: Theme.of(context).colorScheme.primary,
          // ),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(200),
                child: Image.network(
                  "https://cdn.pixabay.com/photo/2021/03/25/14/45/employee-6123303_1280.png",
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(height: 5),
              const Text(
                "Hr Management",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
        drawerButton(
          Icons.movie_edit,
          'Image Editor',
          () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: ((context) => const EditImageFile(selectedImage: "assets/poster1.png",)),
              ),
            );
          },
        ),
        drawerButton(
          Icons.draw,
          'Drawing App',
          () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: ((context) => const DrawingPage()),
              ),
            );
          },
        ),
        drawerButton(
          Icons.draw,
          'Drag Text simple',
          () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: ((context) => const DraggableTextPage()),
              ),
            );
          },
        ),
        drawerButton(
          Icons.draw,
          'Drag Basic',
          () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: ((context) => const DraggableBasicPage()),
              ),
            );
          },
        ),
        drawerButton(
          Icons.draw,
          'Drag Advance',
          () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: ((context) => const DraggableAdvancedPage()),
              ),
            );
          },
        ),
      ],
    ));
  }

  ListTile drawerButton(IconData icon, String title, Function() onTap) {
    return ListTile(
      leading: Icon(
        icon,
        size: 30,
        // color: Colors.orange,
      ),
      onTap: onTap,
      title: Text(
        title,
        style: TextStyle(fontSize: 18, color: Colors.grey.shade800),
      ),
    );
  }
}
