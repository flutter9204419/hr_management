import 'package:image_picker/image_picker.dart';

class ImageDataInfo {
  double left;
  double top;
  XFile file;
  double width;
  double height;

  ImageDataInfo({
    required this.left,
    required this.top,
    required this.file,
    required this.width,
    required this.height,
  });
}
