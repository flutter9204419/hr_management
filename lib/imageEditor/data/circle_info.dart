import 'package:flutter/material.dart';

class CircleInfo {
  double top;
  double height;
  double left;
  double width;
  Color bgColor;
  Color strokeColor;
  double strokeWidth;

  CircleInfo({
    required this.top,
    required this.left,
    required this.height,
    required this.width,
    required this.bgColor,
    required this.strokeColor,
    required this.strokeWidth,
  });
}
