import 'package:flutter/material.dart';

class RectInfo {
  double top;
  double left;
  double height;
  double width;
  Color bgColor;
  Color strokeColor;
  double strokeWidth;

  RectInfo({
    required this.top,
    required this.left,
    required this.height,
    required this.width,
    required this.bgColor,
    required this.strokeColor,
    required this.strokeWidth,
  });
}
