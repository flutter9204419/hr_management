import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:hr_management/imageEditor/data/circle_info.dart';
import 'package:hr_management/imageEditor/data/image_info.dart';
import 'package:hr_management/imageEditor/data/rect_info.dart';
import 'package:hr_management/imageEditor/data/text_info.dart';
import 'package:hr_management/imageEditor/database/app_preferences.dart';
import 'package:hr_management/imageEditor/model/login_model.dart';
import 'package:hr_management/imageEditor/widgets/default_button.dart';
import 'package:hr_management/imageEditor/widgets/image_text.dart';
import 'package:hr_management/utils.dart';
import 'package:image/image.dart' as i;
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

class PosterFinalView extends StatefulWidget {
  final String selectedImage;
  const PosterFinalView({super.key, required this.selectedImage});

  @override
  State<PosterFinalView> createState() => _PosterFinalViewState();
}

class _PosterFinalViewState extends State<PosterFinalView> {
  final TextEditingController textcontroller = TextEditingController();
  final TextEditingController creatorText = TextEditingController();
  ScreenshotController screenshotController = ScreenshotController();
  List texts = [];
  List<ImageDataInfo> images = [];
  int currentIndex = 0;
  List<String> itemList = ["text", "image", "rectangle", "circle"];
  String selectedItem = "";
  List<Color> selectedColorList = [
    Colors.black,
    Colors.black,
    Colors.black,
    Colors.black,
    Colors.blue,
    Colors.green,
  ];
  bool openSlider = false;
  bool bgColorSelected = false;
  bool isGradiant = false;
  bool isGradiantPickerSelected = false;
  Color bgColor = Colors.black;
  String property = "width";
  final GlobalKey widgetKey = GlobalKey();
  final GlobalKey appbarKey = GlobalKey();
  double x = 0;
  double y = 0;
  double position = 0;
  double width = 0;
  double height = 0;

  setCurrentIndex(BuildContext context, int index) {
    setState(() {
      currentIndex = index;
      log(currentIndex.toString());
      texts[currentIndex];
    });
  }

  void onWidgetRendered() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // Find the RenderBox of the widget using the GlobalKey
      RenderBox renderBox =
          widgetKey.currentContext?.findRenderObject() as RenderBox;

      // Get the starting pixel position (x, y)
      final positionWidget = renderBox.localToGlobal(Offset.zero);
      width = renderBox.size.width;
      height = renderBox.size.height;
      log("Widget starting position232342: x=$width, y=$height");

      x = positionWidget.dx;
      // y = positionWidget.dy - (height / 2);
      y = positionWidget.dy;
      position = positionWidget.dy;

      log("Widget starting position1: x=$x, y=$y and postion=$position");
    });
  }

  void showOptions(BuildContext context, Offset position) {
    final RenderBox overlay =
        Overlay.of(context).context.findRenderObject() as RenderBox;

    showMenu(
      context: context,
      position: RelativeRect.fromRect(
        position & const Size(40, 40), // Adjust the size as needed
        Offset.zero & overlay.size,
      ),
      items: <PopupMenuEntry>[
        const PopupMenuItem(
          height: 0,
          value: 'delete',
          child: Row(
            children: [Icon(Icons.delete), Text("Delete")],
          ),
        ),
      ],
    ).then((value) {
      if (value == 'delete') {
        // Handle edit action
        log("Let's Edit");
        removeText();
      }
    });
  }

  saveToGallery(BuildContext context) async {
    if (texts.isNotEmpty || images.isNotEmpty) {
      final double targetPixelRatio =
          1024 / MediaQuery.of(context).size.shortestSide;
      log(targetPixelRatio.toString());
      //1 st Solution
      // screenshotController
      //     .capture(
      //   pixelRatio: 1,
      // )
      //     .then((Uint8List? image) {
      //   saveImage(image!);
      // }).catchError((err) {
      //   log("$err");
      // });
      ///// new code
      ///
      ///2snd Solution
      final imageFile = await screenshotController.capture(pixelRatio: 1.0);

      if (imageFile != null) {
        // Get the directory for local storage
        // final appDirectory = await getApplicationDocumentsDirectory();
        // final filePath = '${appDirectory.path}/my_screenshot.png';

        // Resize the image to 1024x1024
        final originalImage = i.decodeImage(imageFile);
        final resizedImage =
            i.copyResize(originalImage!, width: 1024, height: 1024);

        // Write the resized image to a file
        // File(filePath)
        //     .writeAsBytesSync(i.encodeJpg(resizedImage, quality: 100));
        // await ImageGallerySaver.saveImage(resizedImage, name: filePath);
        final imageBytes =
            Uint8List.fromList(i.encodeJpg(resizedImage, quality: 100));
        final time = DateTime.now()
            .toIso8601String()
            .replaceAll(".", "-")
            .replaceAll(":", "-");
        final name = "screenshot_$time";
        await ImageGallerySaver.saveImage(imageBytes, name: name);

        // ignore: use_build_context_synchronously
        Flushbar(
          message: "Image saved successfully.",
          flushbarPosition: FlushbarPosition.TOP,
          duration: const Duration(seconds: 2),
          backgroundColor: Colors.green,
          margin: const EdgeInsets.all(16),
          borderRadius: BorderRadius.circular(10),
        ).show(context);
      }
      ////
    }
  }

  saveImage(Uint8List bytes) async {
    final time = DateTime.now()
        .toIso8601String()
        .replaceAll(".", "-")
        .replaceAll(":", "-");

    final name = "screenshot_$time";

    await Utils.requestPermission(Permission.storage);
    await ImageGallerySaver.saveImage(bytes, name: name);
  }

  removeText() {
    setState(() {
      texts.removeAt(currentIndex);
      openSlider = false;
    });
  }

  changePositionDown() {
    setState(() {
      log("down call ");
      if (currentIndex != 0) {
        log("down call inside");
        var elementToMove = texts.removeAt(currentIndex);
        texts.insert(currentIndex - 1, elementToMove);
        currentIndex -= 1;
      }
    });
  }

  changePositionUp() {
    log("up call");
    setState(() {
      if (currentIndex != texts.length - 1) {
        log("up call inside");
        var elementToMove = texts.removeAt(currentIndex);
        texts.insert(currentIndex + 1, elementToMove);
        currentIndex += 1;
      }
    });
  }

  chnageTextColor(Color color) {
    setState(() {
      texts[currentIndex].color = color;
    });
  }

  chnageTextBgColor(Color color) {
    setState(() {
      texts[currentIndex].bgColor = color;
    });
  }

  chnageTextStockColor(Color color) {
    setState(() {
      texts[currentIndex].strokeColor = color;
    });
  }

  increaseFont() {
    setState(() {
      texts[currentIndex].fontSize += 2;
    });
  }

  decreaseFont() {
    setState(() {
      if (texts[currentIndex].fontSize > 4) texts[currentIndex].fontSize -= 2;
    });
  }

  alignLeft() {
    setState(() {
      texts[currentIndex].textAlign = TextAlign.left;
    });
  }

  alignCenter() {
    setState(() {
      texts[currentIndex].textAlign = TextAlign.center;
    });
  }

  alignRight() {
    setState(() {
      texts[currentIndex].textAlign = TextAlign.right;
    });
  }

  boldFont() {
    setState(() {
      texts[currentIndex].fontWeight == FontWeight.bold
          ? texts[currentIndex].fontWeight = FontWeight.normal
          : texts[currentIndex].fontWeight = FontWeight.bold;
    });
  }

  styleFont() {
    setState(() {
      texts[currentIndex].fontStyle == FontStyle.italic
          ? texts[currentIndex].fontStyle = FontStyle.normal
          : texts[currentIndex].fontStyle = FontStyle.italic;
    });
  }

  addLineToText() {
    setState(() {
      if (texts[currentIndex].text.contains("\n")) {
        texts[currentIndex].text =
            texts[currentIndex].text.replaceAll("\n", " ");
      } else {
        texts[currentIndex].text =
            texts[currentIndex].text.replaceAll(" ", "\n");
      }
    });
  }

  addNewImage(BuildContext context) async {
    XFile? file;
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text(
          "Add New Image or Logo",
          textAlign: TextAlign.center,
        ),
        content: const Text(
          "Please select specific image",
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          DefaultButton(
            onPressed: () => Navigator.pop(context),
            backgroundColor: Colors.grey,
            child: const Text(
              "Back",
              style: TextStyle(color: Colors.black),
            ),
          ),
          DefaultButton(
            onPressed: () async {
              LoginModel? details = await AppPreferences().getLoginDetails();
              file = XFile(details?.image ?? "");
              log("image $x and  $y");
              log(file.toString());
              if (file != null) {
                setState(() {
                  texts.add(ImageDataInfo(
                      file: file!, top: y, left: x, width: 50, height: 50));
                });
                // ignore: use_build_context_synchronously
                Navigator.pop(context);
              } else {
                log("Something want wrong");
              }
            },
            backgroundColor: Theme.of(context).colorScheme.primary,
            child: const Text(
              "Add Logo",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          DefaultButton(
            onPressed: () async {
              file = await ImagePicker().pickImage(source: ImageSource.gallery);
              log("image $x and  $y");
              log(file.toString());
              if (file != null) {
                setState(() {
                  texts.add(ImageDataInfo(
                      file: file!, top: y, left: x, width: 150, height: 150));
                });
                // ignore: use_build_context_synchronously
                Navigator.pop(context);
              } else {
                log("Something want wrong");
              }
            },
            backgroundColor: Theme.of(context).colorScheme.primary,
            child: const Text(
              "Add Image",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  addNewRect(BuildContext context) async {
    setState(() {
      log("Rect $x and  $y");
      texts.add(
        RectInfo(
            top: y,
            left: x,
            height: 120,
            width: 120,
            bgColor: Colors.transparent,
            strokeColor: Colors.black,
            strokeWidth: 5),
      );
    });
  }

  addNewCircle(BuildContext context) async {
    setState(() {
      log("Circle $x and  $y");
      texts.add(
        CircleInfo(
            top: y,
            left: x,
            height: 120,
            width: 120,
            bgColor: Colors.transparent,
            strokeColor: Colors.black,
            strokeWidth: 5),
      );
    });
  }

  addNewText(BuildContext context) {
    setState(() {
      log("text $x and  $y");
      texts.add(
        TextInfo(
          text: textcontroller.text,
          left: x,
          top: y,
          color: Colors.black,
          fontWeight: FontWeight.normal,
          fontStyle: FontStyle.normal,
          fontSize: 20,
          textAlign: TextAlign.left,
          bgColor: Colors.transparent,
          strokeWidth: 1,
          strokeColor: Colors.transparent,
          width: 300,
        ),
      );
      textcontroller.clear();
      Navigator.pop(context);
    });
  }

  editText() {
    setState(() {
      texts[currentIndex].text = textcontroller.text;
    });
    textcontroller.clear();
    Navigator.pop(context);
  }

  addNewDialog(context, bool isEdit) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(isEdit ? "Edit Text" : "Add New Text"),
        content: TextField(
          controller: textcontroller,
          maxLines: 5,
          decoration: const InputDecoration(
            suffixIcon: Icon(
              Icons.edit,
            ),
            filled: true,
            hintText: "Your new text..",
          ),
        ),
        actions: <Widget>[
          DefaultButton(
            onPressed: () => Navigator.pop(context),
            backgroundColor: Colors.grey,
            child: const Text(
              "Back",
              style: TextStyle(color: Colors.black),
            ),
          ),
          DefaultButton(
            onPressed: () => isEdit ? editText() : addNewText(context),
            backgroundColor: Theme.of(context).colorScheme.primary,
            child: Text(
              isEdit ? "Edit Text" : "Add Text",
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  final GlobalKey _globalKey = GlobalKey();
  late Uint8List _capturedImage;

  Future<void> _captureAndSaveImage() async {
    try {
      // Ensure that the widget is laid out before capturing
      log("Innner");
      if (_globalKey.currentContext != null) {
        final boundary = _globalKey.currentContext?.findRenderObject()
            as RenderRepaintBoundary;
        final image = await boundary.toImage(
            pixelRatio: 1.0); // Adjust pixelRatio as needed
        final byteData = await image.toByteData(format: ImageByteFormat.png);
        final buffer = byteData?.buffer.asUint8List();

        i.Image capturedImage = i.decodeImage(buffer!)!;
        // final resizedImage =
        //     i.copyResize(capturedImage, width: 1024, height: 1024);

        // Encode the image as PNG with high quality (you can also use JPEG)
        final encodedImage = i.encodePng(capturedImage);
        // Save the image to the gallery or a specific location
        // You can use a package like 'image_gallery_saver' or 'path_provider' to save the image
        _saveImageToGallery(encodedImage);

        // Update the state to display the captured image
        setState(() {
          _capturedImage = encodedImage;
        });
      }
    } catch (e) {
      log("Outer");

      log('Failed to capture and save image: $e');
    }
  }

  void _saveImageToGallery(Uint8List imageBytes) async {
    // final imageBytes =
    //         Uint8List.fromList(i.encodeJpg(resizedImage, quality: 100));
    final time = DateTime.now()
        .toIso8601String()
        .replaceAll(".", "-")
        .replaceAll(":", "-");
    final name = "screenshot_$time";
    await ImageGallerySaver.saveImage(imageBytes, name: name);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          openSlider = false;
          property = "";
        });
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 80,
              ),
              // Center(
              //   child: Row(
              //     children: [
              //       IconButton(
              //         icon: const Icon(Icons.upload_file),
              //         onPressed: () async {
              //           file = await ImagePicker()
              //               .pickImage(source: ImageSource.gallery);
              //           setState(() {
              //             bgColorSelected = false;
              //           });
              //         },
              //       ),
              //       IconButton(
              //         icon: const Icon(Icons.color_lens),
              //         onPressed: () {
              //           pickColor(context, 3);
              //           setState(() {
              //             bgColorSelected = true;
              //             isGradiant = false;
              //           });
              //         },
              //       ),
              //       IconButton(
              //         icon: const Icon(Icons.gradient),
              //         onPressed: () {
              //           setState(() {
              //             isGradiantPickerSelected = true;
              //           });
              //           pickColor(context, 4);
              //           setState(() {
              //             isGradiant = true;
              //             // isGradiantPickerSelected = false;
              //           });
              //         },
              //       ),
              //     ],
              //   ),
              // ),
              if (/* file != null*/
                  widget.selectedImage.isNotEmpty)
                Center(
                  // child: RepaintBoundary(
                  // key: _globalKey,
                  child: Screenshot(
                    controller: screenshotController,
                    child: SizedBox(
                      width: 350,
                      height: 350,
                      // height: MediaQuery.of(context).size.height * 0.65,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          // !bgColorSelected && !isGradiant
                          //     ?
                          _selectedImage,
                          // : _selectedColor,
                          for (int i = 0; i < texts.length; i++)
                            if (texts[i] is TextInfo)
                              Positioned(
                                left: texts[i].left,
                                top: texts[i].top,
                                child: GestureDetector(
                                  onDoubleTap: () {
                                    setCurrentIndex(context, i);
                                    textcontroller.text = texts[i].text;
                                    addNewDialog(context, true);
                                  },
                                  onLongPress: () {
                                    setCurrentIndex(context, i);
                                    final RenderBox renderBox =
                                        context.findRenderObject() as RenderBox;
                                    final position = renderBox.localToGlobal(
                                        Offset(
                                            texts[i].left, texts[i].top + 100));
                                    log(position.toString());
                                    showOptions(context, position);
                                  },
                                  onTap: () {
                                    setCurrentIndex(context, i);
                                    setState(() {
                                      openSlider = false;
                                      selectedItem = itemList[0];
                                      selectedColorList[0] = texts[i].color;
                                      selectedColorList[1] = texts[i].bgColor;
                                      selectedColorList[2] =
                                          texts[i].strokeColor;
                                    });
                                  },
                                  child: Draggable(
                                    feedback: ImageText(textInfo: texts[i]),
                                    childWhenDragging: Container(),
                                    child: ImageText(textInfo: texts[i]),
                                    onDragEnd: (drag) {
                                      setCurrentIndex(context, i);
                                      final renderBox = context
                                          .findRenderObject() as RenderBox;
                                      Offset off =
                                          renderBox.globalToLocal(drag.offset);
                                      setState(() {
                                        openSlider = false;
                                        selectedItem = itemList[0];
                                        selectedColorList[0] = texts[i].color;
                                        selectedColorList[1] = texts[i].bgColor;
                                        selectedColorList[2] =
                                            texts[i].strokeColor;
                                        // texts[i].top =
                                        //     off.dy - 138.66666666666856;
                                        texts[i].top = off.dy - position;
                                        texts[i].left = off.dx - x;
                                      });
                                    },
                                  ),
                                ),
                              )
                            else if (texts[i] is ImageDataInfo)
                              Positioned(
                                left: texts[i].left,
                                top: texts[i].top,
                                child: GestureDetector(
                                  onDoubleTap: () {
                                    // setCurrentIndex(context, i);
                                    // textcontroller.text = texts[i].text;
                                    // addNewDialog(context, true);
                                  },
                                  onLongPress: () {
                                    setCurrentIndex(context, i);
                                    log("LongPress");
                                    final RenderBox renderBox =
                                        context.findRenderObject() as RenderBox;
                                    final position = renderBox.localToGlobal(
                                        Offset(
                                            texts[i].left, texts[i].top + 100));
                                    log(position.toString());
                                    showOptions(context, position);
                                  },
                                  onTap: () {
                                    setCurrentIndex(context, i);
                                    setState(() {
                                      selectedItem = itemList[1];
                                    });
                                  },
                                  child: Draggable(
                                    // feedback: Container(),
                                    feedback: Image.file(
                                      File(texts[i].file.path),
                                      fit: BoxFit.fill,
                                      width: texts[i].width,
                                      height: texts[i].height,
                                    ),
                                    childWhenDragging: Container(),
                                    child: Image.file(
                                      File(texts[i].file.path),
                                      fit: BoxFit.fill,
                                      width: texts[i].width,
                                      height: texts[i].height,
                                    ),
                                    onDragEnd: (drag) {
                                      setCurrentIndex(context, i);
                                      final renderBox = context
                                          .findRenderObject() as RenderBox;
                                      Offset off =
                                          renderBox.globalToLocal(drag.offset);
                                      setState(() {
                                        selectedItem = itemList[1];
                                        // texts[i].top = off.dy - 138.66666666666856;
                                        texts[i].top = off.dy - position;
                                        texts[i].left = off.dx - x;
                                      });
                                      log(texts[i].top.toString());
                                      log(off.dy.toString());
                                      log(height.toString());
                                      log(texts[i].left.toString());
                                    },
                                  ),
                                ),
                              )
                            else if (texts[i] is RectInfo)
                              Positioned(
                                left: texts[i].left,
                                top: texts[i].top,
                                child: GestureDetector(
                                  onLongPress: () {
                                    setCurrentIndex(context, i);
                                    log("LongPress");
                                    final RenderBox renderBox =
                                        context.findRenderObject() as RenderBox;
                                    final position = renderBox.localToGlobal(
                                        Offset(
                                            texts[i].left, texts[i].top + 100));
                                    log(position.toString());
                                    showOptions(context, position);
                                  },
                                  onTap: () {
                                    setCurrentIndex(context, i);
                                    setState(() {
                                      selectedItem = itemList[2];
                                      selectedColorList[1] = texts[i].bgColor;
                                      selectedColorList[2] =
                                          texts[i].strokeColor;
                                    });
                                  },
                                  child: Draggable(
                                    // feedback: Container(),
                                    feedback: Container(
                                      width: texts[i].width,
                                      height: texts[i].height,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: texts[i].bgColor,
                                        border: Border.all(
                                          color: texts[i].strokeColor,
                                          width: texts[i].strokeWidth,
                                        ),
                                      ),
                                    ),

                                    childWhenDragging: Container(),
                                    child: Container(
                                      width: texts[i].width,
                                      height: texts[i].height,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: texts[i].bgColor,
                                        border: Border.all(
                                          color: texts[i].strokeColor,
                                          width: texts[i].strokeWidth,
                                        ),
                                      ),
                                    ),
                                    onDragEnd: (drag) {
                                      setCurrentIndex(context, i);
                                      final renderBox = context
                                          .findRenderObject() as RenderBox;
                                      Offset off =
                                          renderBox.globalToLocal(drag.offset);
                                      setState(() {
                                        selectedItem = itemList[2];
                                        selectedColorList[1] = texts[i].bgColor;
                                        selectedColorList[2] =
                                            texts[i].strokeColor;
                                        texts[i].top = off.dy - position;
                                        // texts[i].top =
                                        //     // off.dy - 138.66666666666856;
                                        //     off.dy - 138.66666666666856;
                                        texts[i].left = off.dx - x;
                                      });
                                      log("top ${texts[i].top}");
                                      log("left ${texts[i].left}");
                                      log("dy ${off.dy}");
                                      log("dx ${off.dx}");
                                    },
                                  ),
                                ),
                              )
                            else if (texts[i] is CircleInfo)
                              Positioned(
                                left: texts[i].left,
                                top: texts[i].top,
                                child: GestureDetector(
                                  onLongPress: () {
                                    setCurrentIndex(context, i);
                                    log("LongPress");
                                    final RenderBox renderBox =
                                        context.findRenderObject() as RenderBox;
                                    final position = renderBox.localToGlobal(
                                        Offset(
                                            texts[i].left, texts[i].top + 100));
                                    log(position.toString());
                                    showOptions(context, position);
                                  },
                                  onTap: () {
                                    setCurrentIndex(context, i);
                                    setState(() {
                                      selectedItem = itemList[3];
                                      selectedColorList[1] = texts[i].bgColor;
                                      selectedColorList[2] =
                                          texts[i].strokeColor;
                                    });
                                  },
                                  child: Draggable(
                                    // feedback: Container(),
                                    feedback: Container(
                                      width: texts[i].width,
                                      height: texts[i].height,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: texts[i].bgColor,
                                        border: Border.all(
                                          color: texts[i].strokeColor,
                                          width: texts[i].strokeWidth,
                                        ),
                                      ),
                                    ),

                                    childWhenDragging: Container(),
                                    child: Container(
                                      width: texts[i].width,
                                      height: texts[i].height,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: texts[i].bgColor,
                                        border: Border.all(
                                          color: texts[i].strokeColor,
                                          width: texts[i].strokeWidth,
                                        ),
                                      ),
                                    ),
                                    onDragEnd: (drag) {
                                      setCurrentIndex(context, i);
                                      final renderBox = context
                                          .findRenderObject() as RenderBox;
                                      Offset off =
                                          renderBox.globalToLocal(drag.offset);
                                      setState(() {
                                        selectedItem = itemList[3];
                                        selectedColorList[1] = texts[i].bgColor;
                                        selectedColorList[2] =
                                            texts[i].strokeColor;
                                        // texts[i].top =
                                        //     off.dy - 138.66666666666856;
                                        texts[i].top = off.dy - position;
                                        texts[i].left = off.dx - x;
                                      });
                                      log(texts[i].top.toString());
                                      log(texts[i].left.toString());
                                    },
                                  ),
                                ),
                              ),
                          creatorText.text.isNotEmpty
                              ? Positioned(
                                  left: 0,
                                  bottom: 0,
                                  child: Text(
                                    creatorText.text,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black.withOpacity(0.3),
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink()
                        ],
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
        bottomNavigationBar:
            widget.selectedImage.isNotEmpty || bgColorSelected || isGradiant
                ? _appBar
                : const SizedBox.shrink(),
        floatingActionButton: /* file != null*/
            widget.selectedImage.isNotEmpty || bgColorSelected || isGradiant
                ? openSlider
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text("$property Change"),
                            Slider(
                              value: property == "width"
                                  ? texts[currentIndex].width
                                  : property == "height"
                                      ? texts[currentIndex].height
                                      : property == "strokeWidth"
                                          ? texts[currentIndex].strokeWidth
                                          : "",
                              onChanged: (double value) {
                                setState(() {
                                  if (property == "width") {
                                    texts[currentIndex].width = value;
                                  }
                                  if (property == "height") {
                                    texts[currentIndex].height = value;
                                  }
                                  if (property == "strokeWidth") {
                                    texts[currentIndex].strokeWidth = value;
                                  }
                                });
                              },
                              min: 0,
                              max: property == "strokeWidth" ? 20 : 500,
                            ),
                          ],
                        ),
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          _addAddressFab,
                          const SizedBox(width: 10),
                          _addNewTextFab,
                          const SizedBox(width: 10),
                          _addNewImageFab,
                          const SizedBox(width: 10),
                          _addNewRectangle,
                          const SizedBox(width: 10),
                          _addNewCircle,
                        ],
                      )
                : const SizedBox.shrink(),
      ),
    );
  }

  Widget get _selectedImage => Center(
        child: SizedBox(
          key: widgetKey,
          width: 350,
          height: 350,
          child: Builder(builder: (context) {
            onWidgetRendered();
            return Image.asset(
              widget.selectedImage,
              fit: BoxFit.fill,
              width: MediaQuery.of(context).size.width,
            );
          }),
        ),
        // child: Image.file(
        //   File(file!.path),
        //   fit: BoxFit.fill,
        //   width: MediaQuery.of(context).size.width,
        // ),
      );
  Widget get _selectedColor => Center(
        child: Container(
          key: widgetKey,
          height: 350,
          width: 350,
          decoration: isGradiant
              ? BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      selectedColorList[4],
                      selectedColorList[5],
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.bottomRight,
                    // stops: [0.2, 1],
                    // transform: GradientRotation(2),
                    tileMode: TileMode.mirror,
                  ),
                )
              : BoxDecoration(
                  color: selectedColorList[3],
                ),
          child: Builder(builder: (context) {
            onWidgetRendered();
            return Container();
          }),
        ),
      );

  Widget get _addNewTextFab => FloatingActionButton(
        onPressed: () => addNewDialog(context, false),
        backgroundColor: Colors.white,
        tooltip: 'Add new text',
        child: const Icon(
          Icons.edit,
        ),
      );
  Widget get _addAddressFab => FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text("Add Company Address"),
              content: const Text("Are you sure you want to add address"),
              actions: <Widget>[
                DefaultButton(
                  onPressed: () => Navigator.pop(context),
                  backgroundColor: Colors.grey,
                  child: const Text(
                    "Back",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                DefaultButton(
                  onPressed: () async {
                    LoginModel? details =
                        await AppPreferences().getLoginDetails();
                    // await AppPreferences().clearData();
                    log(details?.address ?? "No");
                    setState(() {
                      log("text $x and  $y");
                      texts.add(
                        TextInfo(
                          text: details?.address ??
                              "Add your Address(Double click to edit text)",
                          left: x,
                          top: y,
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontStyle: FontStyle.normal,
                          fontSize: 20,
                          textAlign: TextAlign.left,
                          bgColor: Colors.transparent,
                          strokeWidth: 1,
                          strokeColor: Colors.transparent,
                          width: 300,
                        ),
                      );
                      textcontroller.clear();
                      Navigator.pop(context);
                    });
                  },
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  child: const Text(
                    "Add Address",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        backgroundColor: Colors.white,
        tooltip: 'Add Address',
        child: const Icon(
          Icons.location_on,
        ),
      );
  Widget get _addNewRectangle => FloatingActionButton(
        onPressed: () => addNewRect(context),
        backgroundColor: Colors.white,
        tooltip: 'Add Rectangle',
        child: const Icon(
          Icons.rectangle_outlined,
        ),
      );
  Widget get _addNewCircle => FloatingActionButton(
        onPressed: () => addNewCircle(context),
        backgroundColor: Colors.white,
        tooltip: 'Add Circle',
        child: const Icon(
          Icons.circle_outlined,
        ),
      );
  Widget get _addNewImageFab => FloatingActionButton(
        onPressed: () => addNewImage(context),
        backgroundColor: Colors.white,
        tooltip: 'Add New Image',
        child: const Icon(
          Icons.image_outlined,
        ),
      );

  Widget get _appBar => SizedBox(
        height: 100,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            appButton(() => saveToGallery(context), Icons.save, "Save Image"),
            // appButton(_captureAndSaveImage, Icons.save, "Save Image"),
            appButton(
                changePositionUp, Icons.arrow_upward_outlined, "Position up"),
            appButton(changePositionDown, Icons.arrow_downward_outlined,
                "Position down"),
            if (selectedItem == itemList[0]) ...[
              appButton(increaseFont, Icons.add, "Increase Size"),
              appButton(decreaseFont, Icons.remove, "Decrease Size"),
              appButton(alignLeft, Icons.format_align_left, "Align Left"),
              appButton(alignCenter, Icons.format_align_center, "Align Center"),
              appButton(alignRight, Icons.format_align_right, "Align Right"),
              appButton(boldFont, Icons.format_bold, "Bold"),
              appButton(styleFont, Icons.format_italic, "Italic"),
              appButton(addLineToText, Icons.space_bar, "Add New Line"),
              colorPickerButton("Text Color", "A", 0),
              const SizedBox(width: 20),
              colorPickerButton("Background Color", "B", 1),
              const SizedBox(width: 20),
              colorPickerButton("Border Color", "S", 2),
              const SizedBox(width: 20),
              widthHeightSet("width", Icons.width_normal),
              const SizedBox(width: 20),
            ],
            if (selectedItem == itemList[1]) ...[
              const SizedBox(width: 20),
              widthHeightSet("width", Icons.width_normal),
              const SizedBox(width: 20),
              widthHeightSet("height", Icons.height),
              const SizedBox(width: 20),
            ],
            if (selectedItem == itemList[2] || selectedItem == itemList[3]) ...[
              colorPickerButton("Background Color", "B", 1),
              const SizedBox(width: 20),
              colorPickerButton("Border Color", "S", 2),
              const SizedBox(width: 20),
              widthHeightSet("width", Icons.width_normal),
              const SizedBox(width: 20),
              widthHeightSet("height", Icons.height),
              const SizedBox(width: 20),
              widthHeightSet("strokeWidth", Icons.line_weight),
              const SizedBox(width: 20),
            ]
          ],
        ),
      );

  GestureDetector widthHeightSet(String value, IconData icon) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (property == value) {
            openSlider = !openSlider;
          }
          property = value;
        });
      },
      child: Icon(icon),
    );
  }

  Widget colorPickerButton(String tooltip, String icon, int type) {
    return Tooltip(
      message: tooltip,
      child: GestureDetector(
        onTap: () {
          pickColor(context, type);
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              icon,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            Container(
              decoration: BoxDecoration(color: selectedColorList[type]),
              width: 25,
              height: 5,
            ),
          ],
        ),
      ),
    );
  }

  IconButton appButton(Function() onPressed, IconData icon, String tooltip) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(
        icon,
        color: Colors.black,
      ),
      tooltip: tooltip,
    );
  }

  Widget buildColorPicker(int type) => ColorPicker(
        pickerColor: selectedColorList[type],
        // enableAlpha: false,
        hexInputBar: true,
        colorPickerWidth: 150,
        // paletteType: PaletteType.hsl,
        // ignore: deprecated_member_use
        showLabel: false,
        // pickerAreaHeightPercent: 1,
        onColorChanged: (Color value) {
          if (type == 0) {
            setState(() {
              selectedColorList[type] = value;
              chnageTextColor(value);
            });
          }
          if (type == 1) {
            setState(() {
              selectedColorList[type] = value;
              chnageTextBgColor(value);
            });
          }
          if (type == 2) {
            setState(() {
              selectedColorList[type] = value;
              chnageTextStockColor(value);
            });
          }
          if (type == 3) {
            setState(() {
              bgColorSelected = true;
              selectedColorList[type] = value;
            });
          }
          if (type == 4) {
            setState(() {
              selectedColorList[type] = value;
            });
          }
          if (type == 5) {
            setState(() {
              selectedColorList[type] = value;
            });
          }
        },
      );
  void pickColor(BuildContext context, int type) {
    showDialog(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: AlertDialog(
              title: const Text("Pick Your Color"),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildColorPicker(type),
                  isGradiantPickerSelected
                      ? buildColorPicker(type + 1)
                      : const SizedBox.shrink(),
                  TextButton(
                    child: const Text(
                      "select",
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      setState(() {
                        isGradiantPickerSelected = false;
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
