import 'package:flutter/material.dart';
import 'package:hr_management/imageEditor/database/app_preferences.dart';
import 'package:hr_management/imageEditor/pages/login_view.dart';
import 'package:hr_management/imageEditor/pages/poster_app_final.dart';

class PosterListingPage extends StatelessWidget {
  const PosterListingPage({super.key});

  void handleClick(int item, BuildContext context) {
    if (item == 1) {
      AppPreferences().clearData();
      // ignore: use_build_context_synchronously
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const LoginView()),
          (route) => false);
    }
    if (item == 0) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Festival Poster"),
        // actions: [
        //   IconButton(
        //     onPressed: () async {
        //       await AppPreferences().clearData();
        //       // ignore: use_build_context_synchronously
        //       Navigator.pushAndRemoveUntil(
        //           context,
        //           MaterialPageRoute(builder: (context) => const LoginView()),
        //           (route) => false);
        //     },
        //     icon: const Icon(Icons.logout),
        //   ),
        //   IconButton(
        //     onPressed: () {},
        //     icon: const Icon(Icons.account_circle_rounded),
        //   ),
        // ],
        actions: <Widget>[
          PopupMenuButton<int>(
            onSelected: (item) => handleClick(item, context),
            itemBuilder: (context) => [
              const PopupMenuItem<int>(value: 0, child: Text('Profile')),
              const PopupMenuItem<int>(value: 1, child: Text('Logout')),
            ],
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "New Festival Poster",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.deepPurple.shade300),
              ),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 200,
                child: ListView.builder(
                    itemCount: 30,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PosterFinalView(
                                        selectedImage:
                                            "assets/poster${(index % 6) + 1}.png",
                                      )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(16),
                            child: Image.asset(
                              "assets/poster${(index % 6) + 1}.png",
                              height: 200,
                              width: 200,
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                "Next Festival Poster",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.deepPurple.shade300),
              ),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 200,
                child: ListView.builder(
                    itemCount: 30,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PosterFinalView(
                                        selectedImage:
                                            "assets/poster${(index % 5) + 2}.png",
                                      )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(16),
                            child: Image.asset(
                              "assets/poster${(index % 5) + 2}.png",
                              height: 200,
                              width: 200,
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                "Upcomming Festival Poster",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.deepPurple.shade300),
              ),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 200,
                child: ListView.builder(
                    itemCount: 30,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PosterFinalView(
                                        selectedImage:
                                            "assets/poster${(index % 4) + 3}.png",
                                      )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(16),
                            child: Image.asset(
                              "assets/poster${(index % 4) + 3}.png",
                              height: 200,
                              // width: 200,
                            ),
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
