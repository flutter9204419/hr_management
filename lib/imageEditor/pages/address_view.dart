import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_management/imageEditor/database/app_preferences.dart';
import 'package:hr_management/imageEditor/database/db_helper.dart';
import 'package:hr_management/imageEditor/pages/main_page.dart';

class AddressView extends StatefulWidget {
  final String email;
  final String password;
  final String name;
  final String logo;

  const AddressView(
      {super.key,
      required this.email,
      required this.password,
      required this.name,
      required this.logo});

  @override
  State<AddressView> createState() => _AddressViewState();
}

class _AddressViewState extends State<AddressView> {
  TextEditingController flatController = TextEditingController();
  TextEditingController lineController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController pinController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AppPreferences appPreferences = AppPreferences();

  void signUp() async {
    if (formKey.currentState!.validate()) {
      String address =
          "${flatController.text}, ${lineController.text}, ${cityController.text}, ${stateController.text} - ${pinController.text}";
      final id = await SQLHelper.createData(
        widget.name,
        address,
        widget.email,
        widget.password,
        widget.logo,
      );
      await appPreferences.setLoginDetails(json.encode({
        "id": id,
        "email": widget.email,
        "password": widget.password,
        "image": widget.logo,
        "address": address
      }));
      debugPrint(appPreferences.getLoginDetails().toString());
      // ignore: use_build_context_synchronously
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const MainPage()),
          (route) => false);
      // ignore: use_build_context_synchronously
      // Utils.showSnackBar(context,
      //     color: Colors.green, text: "Register Successfully.");
    } else {
      // ignore: use_build_context_synchronously
      // Utils.showSnackBar(context,
      //     color: Colors.red, text: "Something went wrong.");
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Form(
                  key: formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: [
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Address",
                              style: TextStyle(
                                  fontSize: 40, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        const SizedBox(height: 10),
                        TextFormField(
                          controller: flatController,
                          validator: (p) {
                            if (p == null || p.isEmpty) {
                              return "This field cannot be empty";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "Flat: B/102",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 2, horizontal: 16),
                            hintStyle: TextStyle(color: Colors.grey.shade500),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        TextFormField(
                          controller: lineController,
                          validator: (p) {
                            if (p == null || p.isEmpty) {
                              return "This field cannot be empty";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "Line",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 2, horizontal: 16),
                            hintStyle: TextStyle(color: Colors.grey.shade500),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        TextFormField(
                          controller: cityController,
                          validator: (p) {
                            if (p == null || p.isEmpty) {
                              return "This field cannot be empty";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "City",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 2, horizontal: 16),
                            hintStyle: TextStyle(color: Colors.grey.shade500),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        TextFormField(
                          controller: stateController,
                          validator: (p) {
                            if (p == null || p.isEmpty) {
                              return "This field cannot be empty";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "State,Country",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 2, horizontal: 16),
                            hintStyle: TextStyle(color: Colors.grey.shade500),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        TextFormField(
                          controller: pinController,
                          validator: (p) {
                            if (p == null || p.isEmpty) {
                              return "This field cannot be empty";
                            } else if (p.length < 6) {
                              return "Please enter valid pin code";
                            }
                            return null;
                          },
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            hintText: "Pin Code",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 2, horizontal: 16),
                            hintStyle: TextStyle(color: Colors.grey.shade500),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 1),
                            ),
                          ),
                        ),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: signUp,
                          child: Container(
                            width: double.infinity,
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.primary,
                                borderRadius: BorderRadius.circular(50)),
                            child: const Text(
                              "SignUp",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
