import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:hr_management/imageEditor/data/circle_info.dart';
import 'package:hr_management/imageEditor/data/image_info.dart';
import 'package:hr_management/imageEditor/data/rect_info.dart';
import 'package:hr_management/imageEditor/data/text_info.dart';
import 'package:hr_management/imageEditor/widgets/edit_image_viewmodel.dart';
import 'package:hr_management/imageEditor/widgets/image_text.dart';
import 'package:image_picker/image_picker.dart';
import 'package:screenshot/screenshot.dart';

class EditImageFile extends StatefulWidget {
  final String selectedImage;
  const EditImageFile({super.key, required this.selectedImage});

  @override
  State<EditImageFile> createState() => _EditImageFileState();
}

class _EditImageFileState extends EditImageViewModel {
  XFile? file;

  void showOptions(BuildContext context, Offset position) {
    final RenderBox overlay =
        Overlay.of(context).context.findRenderObject() as RenderBox;

    showMenu(
      context: context,
      position: RelativeRect.fromRect(
        position & const Size(40, 40), // Adjust the size as needed
        Offset.zero & overlay.size,
      ),
      items: <PopupMenuEntry>[
        const PopupMenuItem(
          height: 0,
          value: 'delete',
          child: Row(
            children: [Icon(Icons.delete), Text("Delete")],
          ),
        ),
      ],
    ).then((value) {
      if (value == 'delete') {
        // Handle edit action
        log("Let's Edit");
        removeText();
      }
    });
  }

  void findWidgetPosition() {
    RenderBox renderBox =
        widgetKey.currentContext?.findRenderObject() as RenderBox;
    final position = renderBox.localToGlobal(Offset.zero);
    final x = position.dx;
    final y = position.dy;

    log("Widget starting position: x=$x, y=$y");
  }

  void onWidgetRendered() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // Find the RenderBox of the widget using the GlobalKey
      RenderBox renderBox =
          widgetKey.currentContext?.findRenderObject() as RenderBox;

      // Get the starting pixel position (x, y)
      final positionWidget = renderBox.localToGlobal(Offset.zero);
      width = renderBox.size.width;
      height = renderBox.size.height;
      log("Widget starting position232342: x=$width, y=$height");

      x = positionWidget.dx;
      y = positionWidget.dy - (height / 2);
      position = positionWidget.dy;

      log("Widget starting position1: x=$x, y=$y and postion=$position");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: /* file != null*/
          widget.selectedImage.isNotEmpty || bgColorSelected || isGradiant
              ? _appBar
              : AppBar(title: const Text("Edit Image")),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.upload_file),
                    onPressed: () async {
                      file = await ImagePicker()
                          .pickImage(source: ImageSource.gallery);
                      setState(() {
                        bgColorSelected = false;
                      });
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.color_lens),
                    onPressed: () {
                      pickColor(context, 3);
                      setState(() {
                        bgColorSelected = true;
                        isGradiant = false;
                      });
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.gradient),
                    onPressed: () {
                      setState(() {
                        isGradiantPickerSelected = true;
                      });
                      pickColor(context, 4);
                      setState(() {
                        isGradiant = true;
                        // isGradiantPickerSelected = false;
                      });
                    },
                  ),
                ],
              ),
            ),
            if (/* file != null*/
                widget.selectedImage.isNotEmpty ||
                    bgColorSelected ||
                    isGradiant)
              Screenshot(
                controller: screenshotController,
                child: SafeArea(
                  child: SizedBox(
                    width: 300,
                    height: 300,
                    // height: MediaQuery.of(context).size.height * 0.65,
                    child: Stack(
                      children: [
                        !bgColorSelected && !isGradiant
                            ? _selectedImage
                            : _selectedColor,
                        // Positioned(top: y, left: x, child: Text("All")),
                        for (int i = 0; i < texts.length; i++)
                          if (texts[i] is TextInfo)
                            Positioned(
                              left: texts[i].left,
                              top: texts[i].top,
                              child: GestureDetector(
                                onDoubleTap: () {
                                  setCurrentIndex(context, i);
                                  textcontroller.text = texts[i].text;
                                  addNewDialog(context, true);
                                },
                                onLongPress: () {
                                  setCurrentIndex(context, i);
                                  final RenderBox renderBox =
                                      context.findRenderObject() as RenderBox;
                                  final position = renderBox.localToGlobal(
                                      Offset(
                                          texts[i].left, texts[i].top + 100));
                                  log(position.toString());
                                  showOptions(context, position);
                                },
                                onTap: () {
                                  setCurrentIndex(context, i);
                                  setState(() {
                                    openSlider = false;
                                    selectedItem = itemList[0];
                                    selectedColorList[0] = texts[i].color;
                                    selectedColorList[1] = texts[i].bgColor;
                                    selectedColorList[2] = texts[i].strokeColor;
                                  });
                                },
                                child: Draggable(
                                  feedback: ImageText(textInfo: texts[i]),
                                  childWhenDragging: Container(),
                                  child: ImageText(textInfo: texts[i]),
                                  onDragEnd: (drag) {
                                    setCurrentIndex(context, i);
                                    final renderBox =
                                        context.findRenderObject() as RenderBox;
                                    Offset off =
                                        renderBox.globalToLocal(drag.offset);
                                    setState(() {
                                      openSlider = false;
                                      selectedItem = itemList[0];
                                      selectedColorList[0] = texts[i].color;
                                      selectedColorList[1] = texts[i].bgColor;
                                      selectedColorList[2] =
                                          texts[i].strokeColor;
                                      // texts[i].top =
                                      //     off.dy - 138.66666666666856;
                                      texts[i].top = off.dy -position;
                                      texts[i].left = off.dx -x;
                                    });
                                  },
                                ),
                              ),
                            )
                          else if (texts[i] is ImageDataInfo)
                            Positioned(
                              left: texts[i].left,
                              top: texts[i].top,
                              child: GestureDetector(
                                onDoubleTap: () {
                                  // setCurrentIndex(context, i);
                                  // textcontroller.text = texts[i].text;
                                  // addNewDialog(context, true);
                                },
                                onLongPress: () {
                                  setCurrentIndex(context, i);
                                  log("LongPress");
                                  final RenderBox renderBox =
                                      context.findRenderObject() as RenderBox;
                                  final position = renderBox.localToGlobal(
                                      Offset(
                                          texts[i].left, texts[i].top + 100));
                                  log(position.toString());
                                  showOptions(context, position);
                                },
                                onTap: () {
                                  setCurrentIndex(context, i);
                                  setState(() {
                                    selectedItem = itemList[1];
                                  });
                                },
                                child: Draggable(
                                  // feedback: Container(),
                                  feedback: Image.file(
                                    File(texts[i].file.path),
                                    fit: BoxFit.fill,
                                    width: texts[i].width,
                                    height: texts[i].height,
                                  ),
                                  childWhenDragging: Container(),
                                  child: Image.file(
                                    File(texts[i].file.path),
                                    fit: BoxFit.fill,
                                    width: texts[i].width,
                                    height: texts[i].height,
                                  ),
                                  onDragEnd: (drag) {
                                    setCurrentIndex(context, i);
                                    final renderBox =
                                        context.findRenderObject() as RenderBox;
                                    Offset off =
                                        renderBox.globalToLocal(drag.offset);
                                    setState(() {
                                      selectedItem = itemList[1];
                                      // texts[i].top = off.dy - 138.66666666666856;
                                      texts[i].top = off.dy - position;
                                      texts[i].left = off.dx - x;
                                    });
                                    log(texts[i].top.toString());
                                    log(off.dy.toString());
                                    log(height.toString());
                                    log(texts[i].left.toString());
                                  },
                                ),
                              ),
                            )
                          else if (texts[i] is RectInfo)
                            Positioned(
                              left: texts[i].left,
                              top: texts[i].top,
                              child: GestureDetector(
                                onLongPress: () {
                                  setCurrentIndex(context, i);
                                  log("LongPress");
                                  final RenderBox renderBox =
                                      context.findRenderObject() as RenderBox;
                                  final position = renderBox.localToGlobal(
                                      Offset(
                                          texts[i].left, texts[i].top + 100));
                                  log(position.toString());
                                  showOptions(context, position);
                                },
                                onTap: () {
                                  setCurrentIndex(context, i);
                                  setState(() {
                                    selectedItem = itemList[2];
                                    selectedColorList[1] = texts[i].bgColor;
                                    selectedColorList[2] = texts[i].strokeColor;
                                  });
                                },
                                child: Draggable(
                                  // feedback: Container(),
                                  feedback: Container(
                                    width: texts[i].width,
                                    height: texts[i].height,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      color: texts[i].bgColor,
                                      border: Border.all(
                                        color: texts[i].strokeColor,
                                        width: texts[i].strokeWidth,
                                      ),
                                    ),
                                  ),

                                  childWhenDragging: Container(),
                                  child: Container(
                                    width: texts[i].width,
                                    height: texts[i].height,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      color: texts[i].bgColor,
                                      border: Border.all(
                                        color: texts[i].strokeColor,
                                        width: texts[i].strokeWidth,
                                      ),
                                    ),
                                  ),
                                  onDragEnd: (drag) {
                                    setCurrentIndex(context, i);
                                    final renderBox =
                                        context.findRenderObject() as RenderBox;
                                    Offset off =
                                        renderBox.globalToLocal(drag.offset);
                                    setState(() {
                                      selectedItem = itemList[2];
                                      selectedColorList[1] = texts[i].bgColor;
                                      selectedColorList[2] =
                                          texts[i].strokeColor;
                                      texts[i].top =
                                          off.dy - position;
                                      // texts[i].top =
                                      //     // off.dy - 138.66666666666856;
                                      //     off.dy - 138.66666666666856;
                                      texts[i].left = off.dx -x;
                                    });
                                    log("top ${texts[i].top}");
                                    log("left ${texts[i].left}");
                                    log("dy ${off.dy}");
                                    log("dx ${off.dx}");
                                  },
                                ),
                              ),
                            )
                          else if (texts[i] is CircleInfo)
                            Positioned(
                              left: texts[i].left,
                              top: texts[i].top,
                              child: GestureDetector(
                                onLongPress: () {
                                  setCurrentIndex(context, i);
                                  log("LongPress");
                                  final RenderBox renderBox =
                                      context.findRenderObject() as RenderBox;
                                  final position = renderBox.localToGlobal(
                                      Offset(
                                          texts[i].left, texts[i].top + 100));
                                  log(position.toString());
                                  showOptions(context, position);
                                },
                                onTap: () {
                                  setCurrentIndex(context, i);
                                  setState(() {
                                    selectedItem = itemList[3];
                                    selectedColorList[1] = texts[i].bgColor;
                                    selectedColorList[2] = texts[i].strokeColor;
                                  });
                                },
                                child: Draggable(
                                  // feedback: Container(),
                                  feedback: Container(
                                    width: texts[i].width,
                                    height: texts[i].height,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: texts[i].bgColor,
                                      border: Border.all(
                                        color: texts[i].strokeColor,
                                        width: texts[i].strokeWidth,
                                      ),
                                    ),
                                  ),

                                  childWhenDragging: Container(),
                                  child: Container(
                                    width: texts[i].width,
                                    height: texts[i].height,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: texts[i].bgColor,
                                      border: Border.all(
                                        color: texts[i].strokeColor,
                                        width: texts[i].strokeWidth,
                                      ),
                                    ),
                                  ),
                                  onDragEnd: (drag) {
                                    setCurrentIndex(context, i);
                                    final renderBox =
                                        context.findRenderObject() as RenderBox;
                                    Offset off =
                                        renderBox.globalToLocal(drag.offset);
                                    setState(() {
                                      selectedItem = itemList[3];
                                      selectedColorList[1] = texts[i].bgColor;
                                      selectedColorList[2] =
                                          texts[i].strokeColor;
                                      // texts[i].top =
                                      //     off.dy - 138.66666666666856;
                                      texts[i].top = off.dy - position;
                                      texts[i].left = off.dx -x;
                                    });
                                    log(texts[i].top.toString());
                                    log(texts[i].left.toString());
                                  },
                                ),
                              ),
                            ),
                        creatorText.text.isNotEmpty
                            ? Positioned(
                                left: 0,
                                bottom: 0,
                                child: Text(
                                  creatorText.text,
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black.withOpacity(0.3),
                                  ),
                                ),
                              )
                            : const SizedBox.shrink()
                      ],
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
      floatingActionButton: /* file != null*/
          widget.selectedImage.isNotEmpty || bgColorSelected || isGradiant
              ? openSlider
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("$property Change"),
                          Slider(
                            value: property == "width"
                                ? texts[currentIndex].width
                                : property == "height"
                                    ? texts[currentIndex].height
                                    : property == "strokeWidth"
                                        ? texts[currentIndex].strokeWidth
                                        : "",
                            onChanged: (double value) {
                              setState(() {
                                if (property == "width") {
                                  texts[currentIndex].width = value;
                                }
                                if (property == "height") {
                                  texts[currentIndex].height = value;
                                }
                                if (property == "strokeWidth") {
                                  texts[currentIndex].strokeWidth = value;
                                }
                              });
                            },
                            min: 0,
                            max: property == "strokeWidth" ? 20 : 500,
                          ),
                        ],
                      ),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        _addNewTextFab,
                        const SizedBox(width: 10),
                        _addNewImageFab,
                        const SizedBox(width: 10),
                        _addNewRectangle,
                        const SizedBox(width: 10),
                        _addNewCircle,
                      ],
                    )
              : const SizedBox.shrink(),
    );
  }

  Widget get _selectedImage => Center(
        child: Container(
          key: widgetKey,
          width: 300,
          height: 300,
          child: Builder(builder: (context) {
            onWidgetRendered();
            return Image.asset(
              widget.selectedImage,
              fit: BoxFit.fill,
              width: MediaQuery.of(context).size.width,
            );
          }),
        ),
        // child: Image.file(
        //   File(file!.path),
        //   fit: BoxFit.fill,
        //   width: MediaQuery.of(context).size.width,
        // ),
      );
  Widget get _selectedColor => Center(
        child: Container(
          key: widgetKey,
          height: 300,
          width: 300,
          decoration: isGradiant
              ? BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      selectedColorList[4],
                      selectedColorList[5],
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.bottomRight,
                    // stops: [0.2, 1],
                    // transform: GradientRotation(2),
                    tileMode: TileMode.mirror,
                  ),
                )
              : BoxDecoration(
                  color: selectedColorList[3],
                ),
          child: Builder(builder: (context) {
            onWidgetRendered();
            return Container();
          }),
        ),
      );

  Widget get _addNewTextFab => FloatingActionButton(
        onPressed: () => addNewDialog(context, false),
        backgroundColor: Colors.white,
        tooltip: 'Add new text',
        child: const Icon(
          Icons.edit,
        ),
      );
  Widget get _addNewRectangle => FloatingActionButton(
        onPressed: () => addNewRect(context),
        backgroundColor: Colors.white,
        tooltip: 'Add Rectangle',
        child: const Icon(
          Icons.rectangle_outlined,
        ),
      );
  Widget get _addNewCircle => FloatingActionButton(
        onPressed: () => addNewCircle(context),
        backgroundColor: Colors.white,
        tooltip: 'Add Circle',
        child: const Icon(
          Icons.circle_outlined,
        ),
      );
  Widget get _addNewImageFab => FloatingActionButton(
        onPressed: () => addNewImage(context),
        backgroundColor: Colors.white,
        tooltip: 'Add New Image',
        child: const Icon(
          Icons.image_outlined,
        ),
      );

  AppBar get _appBar => AppBar(
        title: SizedBox(
          height: 50,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              appButton(() => saveToGallery(context), Icons.save, "Save Image"),
              appButton(
                  changePositionUp, Icons.arrow_upward_outlined, "Position up"),
              appButton(changePositionDown, Icons.arrow_downward_outlined,
                  "Position down"),
              if (selectedItem == itemList[0]) ...[
                appButton(increaseFont, Icons.add, "Increase Size"),
                appButton(decreaseFont, Icons.remove, "Decrease Size"),
                appButton(alignLeft, Icons.format_align_left, "Align Left"),
                appButton(
                    alignCenter, Icons.format_align_center, "Align Center"),
                appButton(alignRight, Icons.format_align_right, "Align Right"),
                appButton(boldFont, Icons.format_bold, "Bold"),
                appButton(styleFont, Icons.format_italic, "Italic"),
                appButton(addLineToText, Icons.space_bar, "Add New Line"),
                colorPickerButton("Text Color", "A", 0),
                const SizedBox(width: 10),
                colorPickerButton("Background Color", "B", 1),
                const SizedBox(width: 10),
                colorPickerButton("Border Color", "S", 2),
                const SizedBox(width: 10),
              ],
              if (selectedItem == itemList[1]) ...[
                const SizedBox(width: 10),
                widthHeightSet("width", Icons.width_normal),
                const SizedBox(width: 10),
                widthHeightSet("height", Icons.height),
                const SizedBox(width: 10),
              ],
              if (selectedItem == itemList[2] ||
                  selectedItem == itemList[3]) ...[
                colorPickerButton("Background Color", "B", 1),
                const SizedBox(width: 10),
                colorPickerButton("Border Color", "S", 2),
                const SizedBox(width: 10),
                widthHeightSet("width", Icons.width_normal),
                const SizedBox(width: 10),
                widthHeightSet("height", Icons.height),
                const SizedBox(width: 10),
                widthHeightSet("strokeWidth", Icons.line_weight),
                const SizedBox(width: 10),
              ]
            ],
          ),
        ),
      );

  GestureDetector widthHeightSet(String value, IconData icon) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (property == value) {
            openSlider = !openSlider;
          }
          property = value;
        });
      },
      child: Icon(icon),
    );
  }

  Widget colorPickerButton(String tooltip, String icon, int type) {
    return Tooltip(
      message: tooltip,
      child: GestureDetector(
        onTap: () {
          pickColor(context, type);
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              icon,
              style: const TextStyle(
                  color: Colors.black, fontWeight: FontWeight.bold),
            ),
            Container(
              decoration: BoxDecoration(color: selectedColorList[type]),
              width: 25,
              height: 5,
            ),
          ],
        ),
      ),
    );
  }

  IconButton appButton(Function() onPressed, IconData icon, String tooltip) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(
        icon,
        color: Colors.black,
      ),
      tooltip: tooltip,
    );
  }

  Widget buildColorPicker(int type) => ColorPicker(
        pickerColor: selectedColorList[type],
        // enableAlpha: false,
        hexInputBar: true,
        colorPickerWidth: 150,
        // paletteType: PaletteType.hsl,
        // ignore: deprecated_member_use
        showLabel: false,
        // pickerAreaHeightPercent: 1,
        onColorChanged: (Color value) {
          if (type == 0) {
            setState(() {
              selectedColorList[type] = value;
              chnageTextColor(value);
            });
          }
          if (type == 1) {
            setState(() {
              selectedColorList[type] = value;
              chnageTextBgColor(value);
            });
          }
          if (type == 2) {
            setState(() {
              selectedColorList[type] = value;
              chnageTextStockColor(value);
            });
          }
          if (type == 3) {
            setState(() {
              bgColorSelected = true;
              selectedColorList[type] = value;
            });
          }
          if (type == 4) {
            setState(() {
              selectedColorList[type] = value;
            });
          }
          if (type == 5) {
            setState(() {
              selectedColorList[type] = value;
            });
          }
        },
      );
  void pickColor(BuildContext context, int type) {
    showDialog(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: AlertDialog(
              title: const Text("Pick Your Color"),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildColorPicker(type),
                  isGradiantPickerSelected
                      ? buildColorPicker(type + 1)
                      : const SizedBox.shrink(),
                  TextButton(
                    child: const Text(
                      "select",
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      setState(() {
                        isGradiantPickerSelected = false;
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
