// ignore_for_file: use_build_context_synchronously

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:hr_management/imageEditor/database/app_preferences.dart';
import 'package:hr_management/imageEditor/model/login_model.dart';
import 'package:hr_management/imageEditor/pages/login_view.dart';
import 'package:hr_management/imageEditor/pages/main_page.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () async {
      LoginModel? details = await AppPreferences().getLoginDetails();
      // await AppPreferences().clearData();
      log(details?.image ?? "No");
      if (details != null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const MainPage()),
            (route) => false);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const LoginView()),
            (route) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(color: Colors.white),
        child: const Center(
          child: Text(
            "Splash screen",
            style: TextStyle(fontSize: 40),
          ),
        ),
      ),
    );
  }
}
