// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:hr_management/widget/common_widget.dart';
import 'package:image_picker/image_picker.dart';

class ImageEditor extends StatelessWidget {
  const ImageEditor({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(color: Colors.deepPurple),
      ),
      drawer: const CommonDrawer(),
      body: Center(
        child: IconButton(
          icon: const Icon(Icons.upload_file),
          onPressed: () async {
            XFile? file =
                await ImagePicker().pickImage(source: ImageSource.gallery);
            if (file != null) {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) =>
              //             EditImageFile(selectedImage: file.path.toString())));
            }
          },
        ),
      ),
    );
  }
}
