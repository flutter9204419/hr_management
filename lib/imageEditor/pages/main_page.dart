import 'package:flutter/material.dart';
import 'package:hr_management/drag&Drop/pages/draggable_advanced_page.dart';
import 'package:hr_management/drag&Drop/pages/draggable_basic_page.dart';
import 'package:hr_management/drag&Drop/pages/draggable_text_page.dart';
import 'package:hr_management/drawingApp/pages/drawing_page.dart';
import 'package:hr_management/imageEditor/pages/poster_listing_view.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int index = 0;

  @override
  Widget build(BuildContext context) => Scaffold(
        body: IndexedStack(
          index: index,
          children: pages,
        ),
        bottomNavigationBar: buildBottomBar(),
      );

  Widget buildBottomBar() {
    // const style = TextStyle(color: Colors.white);

    return BottomNavigationBar(
      backgroundColor: Theme.of(context).colorScheme.primary.withOpacity(0.8),
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white54,
      currentIndex: index,
      type: BottomNavigationBarType.fixed,
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.draw_rounded),
          label: 'Draw App',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.edit_attributes),
          label: 'Image Editor',
        ),
        // BottomNavigationBarItem(
        //   icon: Icon(Icons.drag_handle),
        //   label: 'Basic',
        // ),
        // BottomNavigationBarItem(
        //   icon: Icon(Icons.drag_handle),
        //   label: 'Advanced',
        // ),
      ],
      onTap: (int index) => setState(() => this.index = index),
    );
  }

  List<Widget> pages = [
    const DrawingPage(),
    // const EditImageFile(),
    const PosterListingPage(),
    // const LoginView(),
    // const DraggableTextPage(),
    // const DraggableBasicPage(),
    // const DraggableAdvancedPage(),
  ];
  Widget buildPages() {
    switch (index) {
      case 0:
        return const DraggableTextPage();
      case 1:
        return const DraggableBasicPage();
      case 2:
        return const DraggableAdvancedPage();

      default:
        return Container();
    }
  }
}
