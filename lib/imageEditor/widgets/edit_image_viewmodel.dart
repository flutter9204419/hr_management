import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hr_management/imageEditor/data/circle_info.dart';
import 'package:hr_management/imageEditor/data/image_info.dart';
import 'package:hr_management/imageEditor/data/rect_info.dart';
import 'package:hr_management/imageEditor/data/text_info.dart';
import 'package:hr_management/imageEditor/pages/edit_image_final.dart';
import 'package:hr_management/utils.dart';
import 'package:hr_management/imageEditor/widgets/default_button.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

abstract class EditImageViewModel extends State<EditImageFile> {
  final TextEditingController textcontroller = TextEditingController();
  final TextEditingController creatorText = TextEditingController();
  ScreenshotController screenshotController = ScreenshotController();
  List texts = [];
  List<ImageDataInfo> images = [];
  int currentIndex = 0;
  List<String> itemList = ["text", "image", "rectangle", "circle"];
  String selectedItem = "";
  List<Color> selectedColorList = [
    Colors.black,
    Colors.black,
    Colors.black,
    Colors.black,
    Colors.blue,
    Colors.green,
  ];
  bool openSlider = false;
  bool bgColorSelected = false;
  bool isGradiant = false;
  bool isGradiantPickerSelected = false;
  Color bgColor = Colors.black;
  String property = "width";
  final GlobalKey widgetKey = GlobalKey();
  final GlobalKey appbarKey = GlobalKey();
  double x = 0;
  double y = 0;
  double position = 0;
  double width = 0;
  double height = 0;
  setCurrentIndex(BuildContext context, int index) {
    setState(() {
      currentIndex = index;
      log(currentIndex.toString());
      texts[currentIndex];
    });
  }

  saveToGallery(BuildContext context) {
    if (texts.isNotEmpty || images.isNotEmpty) {
      screenshotController.capture().then((Uint8List? image) {
        saveImage(image!);
      }).catchError((err) {
        log("$err");
      });
    }
  }

  saveImage(Uint8List bytes) async {
    final time = DateTime.now()
        .toIso8601String()
        .replaceAll(".", "-")
        .replaceAll(":", "-");

    final name = "screenshot_$time";

    await Utils.requestPermission(Permission.storage);
    await ImageGallerySaver.saveImage(bytes, name: name);
  }

  removeText() {
    setState(() {
      texts.removeAt(currentIndex);
      openSlider = false;
    });
  }

  changePositionDown() {
    setState(() {
      log("down call ");
      if (currentIndex != 0) {
        log("down call inside");
        var elementToMove = texts.removeAt(currentIndex);
        texts.insert(currentIndex - 1, elementToMove);
        currentIndex -= 1;
      }
    });
  }

  changePositionUp() {
    log("up call");
    setState(() {
      if (currentIndex != texts.length - 1) {
        log("up call inside");
        var elementToMove = texts.removeAt(currentIndex);
        texts.insert(currentIndex + 1, elementToMove);
        currentIndex += 1;
      }
    });
  }

  chnageTextColor(Color color) {
    setState(() {
      texts[currentIndex].color = color;
    });
  }

  chnageTextBgColor(Color color) {
    setState(() {
      texts[currentIndex].bgColor = color;
    });
  }

  chnageTextStockColor(Color color) {
    setState(() {
      texts[currentIndex].strokeColor = color;
    });
  }

  increaseFont() {
    setState(() {
      texts[currentIndex].fontSize += 2;
    });
  }

  decreaseFont() {
    setState(() {
      if (texts[currentIndex].fontSize > 4) texts[currentIndex].fontSize -= 2;
    });
  }

  alignLeft() {
    setState(() {
      texts[currentIndex].textAlign = TextAlign.left;
    });
  }

  alignCenter() {
    setState(() {
      texts[currentIndex].textAlign = TextAlign.center;
    });
  }

  alignRight() {
    setState(() {
      texts[currentIndex].textAlign = TextAlign.right;
    });
  }

  boldFont() {
    setState(() {
      texts[currentIndex].fontWeight == FontWeight.bold
          ? texts[currentIndex].fontWeight = FontWeight.normal
          : texts[currentIndex].fontWeight = FontWeight.bold;
    });
  }

  styleFont() {
    setState(() {
      texts[currentIndex].fontStyle == FontStyle.italic
          ? texts[currentIndex].fontStyle = FontStyle.normal
          : texts[currentIndex].fontStyle = FontStyle.italic;
    });
  }

  addLineToText() {
    setState(() {
      if (texts[currentIndex].text.contains("\n")) {
        texts[currentIndex].text =
            texts[currentIndex].text.replaceAll("\n", " ");
      } else {
        texts[currentIndex].text =
            texts[currentIndex].text.replaceAll(" ", "\n");
      }
    });
  }

  addNewImage(BuildContext context) async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    log("image $x and  $y");
    setState(() {
      texts.add(
          ImageDataInfo(file: file!, top: y, left: 0, width: 150, height: 150));
    });
  }

  addNewRect(BuildContext context) async {
    setState(() {
      log("Rect $x and  $y");
      texts.add(
        RectInfo(
            top: y,
            left: 0,
            height: 120,
            width: 120,
            bgColor: Colors.transparent,
            strokeColor: Colors.black,
            strokeWidth: 5),
      );
    });
  }

  addNewCircle(BuildContext context) async {
    setState(() {
      log("Circle $x and  $y");
      texts.add(
        CircleInfo(
            top: y,
            left: 0,
            height: 120,
            width: 120,
            bgColor: Colors.transparent,
            strokeColor: Colors.black,
            strokeWidth: 5),
      );
    });
  }

  addNewText(BuildContext context) {
    setState(() {
      log("text $x and  $y");
      texts.add(
        TextInfo(
          text: textcontroller.text,
          left: 0,
          top: y,
          color: Colors.black,
          fontWeight: FontWeight.normal,
          fontStyle: FontStyle.normal,
          fontSize: 20,
          textAlign: TextAlign.left,
          bgColor: Colors.transparent,
          strokeWidth: 1,
          strokeColor: Colors.transparent,
          width: 200,
        ),
      );
      textcontroller.clear();
      Navigator.pop(context);
    });
  }

  editText() {
    setState(() {
      texts[currentIndex].text = textcontroller.text;
    });
    textcontroller.clear();
    Navigator.pop(context);
  }

  addNewDialog(context, bool isEdit) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(isEdit ? "Edit Text" : "Add New Text"),
        content: TextField(
          controller: textcontroller,
          maxLines: 5,
          decoration: const InputDecoration(
            suffixIcon: Icon(
              Icons.edit,
            ),
            filled: true,
            hintText: "Your new text..",
          ),
        ),
        actions: <Widget>[
          DefaultButton(
            onPressed: () => Navigator.pop(context),
            backgroundColor: Colors.grey,
            child: const Text(
              "Back",
              style: TextStyle(color: Colors.black),
            ),
          ),
          DefaultButton(
            onPressed: () => isEdit ? editText() : addNewText(context),
            backgroundColor: Theme.of(context).colorScheme.primary,
            child: Text(
              isEdit ? "Edit Text" : "Add Text",
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
