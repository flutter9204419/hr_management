import 'package:flutter/material.dart';
import 'package:hr_management/imageEditor/data/text_info.dart';

class ImageText extends StatelessWidget {
  final TextInfo textInfo;
  const ImageText({super.key, required this.textInfo});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        // color: textInfo.bgColor,
        width: textInfo.width,

        decoration: BoxDecoration(
          border: Border.all(
            color: textInfo.strokeColor,
            width: textInfo.strokeWidth,
          ),
          color: textInfo.bgColor,
        ),
        child: Text(
          textInfo.text,
          textAlign: textInfo.textAlign,
          style: TextStyle(
            fontSize: textInfo.fontSize,
            fontStyle: textInfo.fontStyle,
            fontWeight: textInfo.fontWeight,
            color: textInfo.color,
          ),
        ),
      ),
    );
  }
}
