class LoginModel {
  int? id;
  String? email;
  String? password;
  String? name;
  String? image;
  String? address;
  String? createdAt;

  LoginModel(
      {this.email,
      this.password,
      this.id,
      this.image,
      this.createdAt,
      this.name,
      this.address});

  LoginModel.fromJson(Map<String, dynamic> json) {
    email = json['email'].toString();
    id = json['id'];
    image = json['image'];
    password = json['password'];
    name = json['name'];
    address = json['address'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['id'] = id;
    data['image'] = image;
    data['password'] = password;
    data['name'] = name;
    data['address'] = address;
    data['createdAt'] = createdAt;
    return data;
  }
}
