import 'package:flutter/material.dart';
import 'package:hr_management/imageEditor/model/login_model.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""
  CREATE TABLE IF NOT EXISTS client(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT,
    email TEXT,
    password TEXT,
    image TEXT,
    address TEXT,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  )
 """);
    // await database.execute("""DROP TABLE IF EXISTS user""");
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      "demo.db",
      version: 1,
      onCreate: (database, version) async {
        await createTables(database);
      },
    );
  }

  static Future<int> createData(String name, String address, String email,
      String password, String image) async {
    final db = await SQLHelper.db();
    final data = {
      'name': name,
      'address': address,
      'email': email,
      'password': password,
      'image': image,
    };

    final id = await db.insert("client", data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<List<Map<String, dynamic>>> getAllUser() async {
    final db = await SQLHelper.db();
    return db.query("client", orderBy: 'id DESC');
  }

  static Future<List<Map<String, dynamic>>> getSingleUser(int id) async {
    final db = await SQLHelper.db();
    return db.query("client", where: "id = ?", whereArgs: [id], limit: 1);
  }

  static Future<int> updateData(int id, String imagePath) async {
    final db = await SQLHelper.db();
    final data = {"image": imagePath, "createdAt": DateTime.now().toString()};
    final result =
        await db.update("client", data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  // login Detaails
  static Future<LoginModel?> loginDetail(String email, String password) async {
    final db = await SQLHelper.db();
    final res = await db.rawQuery(
        "SELECT * FROM client WHERE email = '$email' and password = '$password'");
    if (res.isNotEmpty) {
      return LoginModel.fromJson(res.first);
    }
    return null;
  }

  static Future<void> deleteData(int id) async {
    final db = await SQLHelper.db();
    try {
      await db.delete("client", where: "id = ?", whereArgs: [id]);
    } catch (e) {
      debugPrint("error when deleting data id:  $id");
    }
  }
}
