import 'dart:convert';
import 'dart:developer';

import 'package:hr_management/imageEditor/model/login_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static final AppPreferences _instance = AppPreferences._internal();

  factory AppPreferences() => _instance;

  AppPreferences._internal();

  //------------------------- Preference Constants -----------------------------
  static const String keyLoginDetails = "keyLoginDetails";
  static const String keyApiToken = "keyApiToken";

  // Method to get login details
  Future<LoginModel?> getLoginDetails() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? loginDetails = prefs.getString(keyLoginDetails);
    if (loginDetails == null) {
      return null;
    }
    try {
      return LoginModel.fromJson(json.decode(loginDetails));
    } catch (e) {
      // printf("App Preference error: ${e.toString()}");
      log("Error $e");
    }
    return null;
  }

  // Method to set login details
  Future<bool> setLoginDetails(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(keyLoginDetails, value);
  }

  Future<void> clearData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove(keyLoginDetails);
  }

  Future<void> saveApiToken({String? value}) async {
    final SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setString(keyApiToken, value!);
  }

  Future<String> getApiToken() async {
    final SharedPreferences shared = await SharedPreferences.getInstance();
    return shared.getString(keyApiToken)!;
  }
}
