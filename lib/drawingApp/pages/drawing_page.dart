// ignore_for_file: library_private_types_in_public_api

import 'dart:async';
import 'dart:developer';
import 'dart:ui' as ui;

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:hr_management/drawingApp/data/drawn_line.dart';
import 'package:hr_management/drawingApp/widgets/sketcher.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

class DrawingPage extends StatefulWidget {
  const DrawingPage({super.key});

  @override
  _DrawingPageState createState() => _DrawingPageState();
}

class _DrawingPageState extends State<DrawingPage> {
  final GlobalKey _globalKey = GlobalKey();
  List<DrawnLine> lines = <DrawnLine>[];
  Color selectedColor = Colors.black;
  double selectedWidth = 5.0;
  DrawnLine line = DrawnLine([const Offset(0, 0)], Colors.black, 5);

  StreamController<List<DrawnLine>> linesStreamController =
      StreamController<List<DrawnLine>>.broadcast();
  StreamController<DrawnLine> currentLineStreamController =
      StreamController<DrawnLine>.broadcast();

  Future<void> save() async {
    try {
      RenderRepaintBoundary boundary = _globalKey.currentContext!
          .findRenderObject() as RenderRepaintBoundary;
      ui.Image image = await boundary.toImage();
      ByteData? byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData!.buffer.asUint8List();
      var saved = await ImageGallerySaver.saveImage(
        pngBytes,
        quality: 100,
        name: DateTime.now().toIso8601String(),
        isReturnImagePathOfIOS: true,
      );
      log(saved.toString());
      // ignore: use_build_context_synchronously
      Flushbar(
        message: saved["errorMessage"] != null
            ? "Error : ${saved["errorMessage"]}"
            : "File saved to the path : ${saved["filePath"]}",
        flushbarPosition: FlushbarPosition.TOP,
        duration: const Duration(seconds: 2),
        backgroundColor: Colors.green,
        margin: const EdgeInsets.all(16),
        borderRadius: BorderRadius.circular(10),
      ).show(context);
    } catch (e) {
      // ignore: use_build_context_synchronously
      Flushbar(
        message: "File saved to the path : $e",
        flushbarPosition: FlushbarPosition.TOP,
        duration: const Duration(seconds: 2),
        backgroundColor: Colors.green,
        margin: const EdgeInsets.all(16),
        borderRadius: BorderRadius.circular(10),
      ).show(context);
      log(e.toString());
    }
  }

  Future<void> clear() async {
    setState(() {
      lines = [];
      line = DrawnLine([const Offset(0, 0)], selectedColor, selectedWidth);
    });
  }

  void clearLast() {
    setState(() {
      log("lines ${lines.length}");
      log("lines $lines");
      lines = List.from(lines)..removeLast();
      linesStreamController.add(lines);
      line = DrawnLine([const Offset(0, 0)], selectedColor, selectedWidth);
      currentLineStreamController.add(line);
      // currentLineStreamController
      log("lines $lines");
      log("lines ${lines.length}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Drawing App"),
      ),
      // drawer: const CommonDrawer(),

      body: Stack(
        children: [
          buildAllPaths(context),
          buildCurrentPath(context),
          buildStrokeToolbar(),
          buildColorToolbar(),
        ],
      ),
    );
  }

  Widget buildCurrentPath(BuildContext context) {
    return GestureDetector(
      onPanStart: onPanStart,
      onPanUpdate: onPanUpdate,
      onPanEnd: onPanEnd,
      child: RepaintBoundary(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.all(4.0),
          color: Colors.transparent,
          alignment: Alignment.topLeft,
          child: StreamBuilder<DrawnLine>(
            stream: currentLineStreamController.stream,
            builder: (context, snapshot) {
              return CustomPaint(
                painter: Sketcher(
                  lines: [line],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildAllPaths(BuildContext context) {
    return RepaintBoundary(
      key: _globalKey,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.transparent,
        padding: const EdgeInsets.all(4.0),
        alignment: Alignment.topLeft,
        child: StreamBuilder<List<DrawnLine>>(
          stream: linesStreamController.stream,
          builder: (context, snapshot) {
            return CustomPaint(
              painter: Sketcher(
                lines: lines,
              ),
            );
          },
        ),
      ),
    );
  }

  void onPanStart(DragStartDetails details) {
    RenderBox box = _globalKey.currentContext!.findRenderObject()! as RenderBox;

    Offset point = box.globalToLocal(details.globalPosition);
    line = DrawnLine([point], selectedColor, selectedWidth);
  }

  void onPanUpdate(DragUpdateDetails details) {
    RenderBox box = _globalKey.currentContext!.findRenderObject()! as RenderBox;
    Offset point = box.globalToLocal(details.globalPosition);

    List<Offset> path = List.from(line.path)..add(point);
    line = DrawnLine(path, selectedColor, selectedWidth);
    currentLineStreamController.add(line);
  }

  void onPanEnd(DragEndDetails details) {
    lines = List.from(lines)..add(line);

    linesStreamController.add(lines);
    setState(() {});
  }

  Widget buildStrokeToolbar() {
    return Positioned(
      bottom: 0.0,
      right: 10.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          buildStrokeButton(5.0),
          buildStrokeButton(10.0),
          buildStrokeButton(15.0),
          buildStrokeButton(20.0),
        ],
      ),
    );
  }

  Widget buildStrokeButton(double strokeWidth) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedWidth = strokeWidth;
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Container(
          width: strokeWidth * 2,
          height: strokeWidth * 2,
          decoration: BoxDecoration(
              color: selectedColor, borderRadius: BorderRadius.circular(50.0)),
        ),
      ),
    );
  }

  Widget buildColorToolbar() {
    return Positioned(
      top: 10.0,
      right: 10.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          lines.isNotEmpty
              ? GestureDetector(
                  onTap: clearLast,
                  child: CircleAvatar(
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    child: const Icon(
                      Icons.undo,
                      size: 20.0,
                      color: Colors.white,
                    ),
                  ),
                )
              : const SizedBox.shrink(),
          const Divider(
            height: 5.0,
          ),
          buildClearButton(),
          const Divider(
            height: 5.0,
          ),
          buildSaveButton(),
          const Divider(
            height: 10.0,
          ),
          buildColorButton(Colors.red),
          buildColorButton(Colors.blueAccent),
          buildColorButton(Colors.deepOrange),
          buildColorButton(Colors.green),
          buildColorButton(Colors.lightBlue),
          buildColorButton(Colors.black),
          buildColorButton(Colors.white),
        ],
      ),
    );
  }

  Widget buildColorButton(Color color) {
    return FloatingActionButton(
      mini: true,
      backgroundColor: color,
      child: Container(),
      onPressed: () {
        setState(() {
          selectedColor = color;
        });
      },
    );
  }

  Widget buildSaveButton() {
    return GestureDetector(
      onTap: save,
      child: CircleAvatar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        child: const Icon(
          Icons.save,
          size: 20.0,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget buildClearButton() {
    return GestureDetector(
      onTap: clear,
      child: CircleAvatar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        child: const Icon(
          Icons.create,
          size: 20.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
